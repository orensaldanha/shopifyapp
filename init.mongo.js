const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
 
// Connection URL
const url = 'mongodb://orensaldanha:ilovea380@ds163769.mlab.com:63769/convert';
 
// Database Name
const dbName = 'convert';
 
// Use connect method to connect to the server
MongoClient.connect(url, function(err, client) {
  assert.equal(null, err);
  console.log("Connected successfully to server");
 
  const db = client.db(dbName);
 
  insertDocuments(db, function() {
    client.close();
  });
});

const insertDocuments = function(db, callback) {
    // Get the documents collection
    const collection = db.collection('customers');
    // Insert some documents
    collection.insertMany([
        { firstName: 'John', lastName: 'Doe'},
        { firstName: 'Brad', lastName: 'Traversy'},
        { firstName: 'Mary', lastName: 'Swanson'},
      ], function(err, result) {
      console.log("Inserted 3 documents into the collection");
      callback(result);
    });
  }