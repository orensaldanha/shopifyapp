require('dotenv').load();
const express = require('express');
const path = require('path');
const session = require('express-session');
const passport = require('passport');
const Auth0Strategy = require('passport-auth0');
const flash = require('connect-flash');
const MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId
const bodyParser = require('body-parser');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const crypto = require('crypto');
const cookie = require('cookie');
const nonce = require('nonce')();
const querystring = require('querystring');
const request = require('request-promise');
const axios = require('axios');

var stringify = require('json-stringify-safe');

const apiKey = process.env.SHOPIFY_API_KEY;
const apiSecret = process.env.SHOPIFY_API_SECRET;
const scopes = ['read_products','read_customers','read_script_tags','write_script_tags'];
const forwardingAddress =  process.env.forwarding_address;

let db;
let access;
let store;

const dbUrl = 'mongodb://orensaldanha:ilovea380@ds163769.mlab.com:63769/convert';
const dbName = 'convert';


// This will configure Passport to use Auth0
const strategy = new Auth0Strategy(
  {
    domain: 'convertilicious.auth0.com',
    clientID: 'QcuCSDY5sqzOUKtpNszTwTxNSQiPzZan',
    clientSecret: '0x-izQKIyWxMHN1sN0ciFsyeXFiP_p2vL6-dIT4AoOg3OdX45AH72GdLQTBA2U3E',
    callbackURL: process.env.CALLBACK_URL || 'https://orenapp.herokuapp.com/callback'
  },
  function(accessToken, refreshToken, extraParams, profile,done) {
    // accessToken is the token to call Auth0 API (not needed in the most cases)
    // extraParams.id_token has the JSON Web Token
    // profile has all the information from the user
    return done(null, profile);
  }
);

passport.use(strategy);

// you can use this section to keep a smaller payload
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

const app = express();

app.use(
  session({
    secret: 'shhhhhhhhh',
    resave: true,
    saveUninitialized: true
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// Handle auth failure error messages
app.use(function(req, res, next) {
 if (req && req.query && req.query.error) {
   req.flash("error", req.query.error);
 }
 if (req && req.query && req.query.error_description) {
   req.flash("error_description", req.query.error_description);
 }
 next();
});

// Check logged in
app.use(function(req, res, next) {
  res.locals.loggedIn = false;
  if (req.session.passport && typeof req.session.passport.user != 'undefined') {
    res.locals.loggedIn = true;
  }
  next();
});

const env = {
  AUTH0_CLIENT_ID: 'QcuCSDY5sqzOUKtpNszTwTxNSQiPzZan',
  AUTH0_DOMAIN: 'convertilicious.auth0.com',
  AUTH0_CALLBACK_URL: process.env.CALLBACK_URL || 'https://orenapp.herokuapp.com/callback'
};

app.get(
  '/login',
  passport.authenticate('auth0', {
    clientID: env.AUTH0_CLIENT_ID,
    domain: env.AUTH0_DOMAIN,
    redirectUri: env.AUTH0_CALLBACK_URL,
    responseType: 'code',
    scope: 'openid',
    prompt: 'none',
  }),
  function(req, res) {
    res.redirect('/');
  }
);

// Perform session logout and redirect to homepage
app.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});

// Perform the final stage of authentication and redirect to '/user'
app.get(
  '/callback',
  passport.authenticate('auth0', {
    failureRedirect: '/login'
  }),
  function(req, res) {
    res.redirect(req.session.returnTo || '/');
  }
);


app.get('/cus', ensureLoggedIn, function(req, res, next) {
  res.redirect('/user');
});

app.get('/oren', ensureLoggedIn, function(req, res, next) {
  res.redirect('/user');
});

//shopify get store route 
app.get('/shopify/installshopify', ensureLoggedIn,(req, res) => {
  res.sendFile(path.join(__dirname+'/integrations/installshopify.html'));
});

//shopify install route
//https:// + shop +/admin/oauth/authorize?client_id=9ecfc884e3ae8ad6d7804b93f3273cfe&scope=read_products +'&state=' + state +'&redirect_uri=' + redirectUri;
app.get('/shopify',ensureLoggedIn, (req, res) => {
  const shop = req.query.shop;
  if (shop) {
    const state = nonce();
    const redirectUri = forwardingAddress + '/shopify/callback';
    const installUrl = 'https://' + shop +
      '/admin/oauth/authorize?client_id=' + apiKey +
      '&scope=' + scopes +
      '&state=' + state +
      '&redirect_uri=' + redirectUri;

    res.cookie('state', state);
    res.redirect(installUrl);
  } else {
    return res.status(400).send('Missing shop parameter. Please add ?shop=your-development-shop.myshopify.com to your request');
  }
});

//shopify install callback route

app.get('/shopify/callback',ensureLoggedIn, (req, res) => {
  const userId = req.user.id.slice(6);
  const { shop, hmac, code, state } = req.query;
  const stateCookie = cookie.parse(req.headers.cookie).state;

  if (state !== stateCookie) {
    return res.status(403).send('Request origin cannot be verified');
  }

  if (shop && hmac && code) {
    // DONE: Validate request is from Shopify
    const map = Object.assign({}, req.query);
    delete map['signature'];
    delete map['hmac'];
    const message = querystring.stringify(map);
    const providedHmac = Buffer.from(hmac, 'utf-8');
    const generatedHash = Buffer.from(
      crypto
        .createHmac('sha256', apiSecret)
        .update(message)
        .digest('hex'),
        'utf-8'
      );
    let hashEquals = false;

    try {
      hashEquals = crypto.timingSafeEqual(generatedHash, providedHmac)
    } catch (e) {
      hashEquals = false;
    };

    if (!hashEquals) {
      return res.status(400).send('HMAC validation failed');
    }

    // DONE: Exchange temporary code for a permanent access token
    const accessTokenRequestUrl = 'https://' + shop + '/admin/oauth/access_token';
    const accessTokenPayload = {
      client_id: apiKey,
      client_secret: apiSecret,
      code,
    };

    request.post(accessTokenRequestUrl, { json: accessTokenPayload })
    .then((accessTokenResponse) => {
      const accessToken = accessTokenResponse.access_token;
      users.updateOne({"_id" : ObjectId(userId)}, {$push: {"access_token": accessToken}});
  
      // DONE: Use access token to make API call to 'shop' endpoint
      const shopRequestUrl = 'https://' + shop + '/admin/shop.json';
      const shopRequestHeaders = {
        'X-Shopify-Access-Token': accessToken,
      };
    
      request.get(shopRequestUrl, { headers: shopRequestHeaders })
      .then((shopResponse) => {
        res.status(200).end(shopResponse);
      })
      .catch((error) => {
        res.status(error.statusCode).send(error.error.error_description);
      });
    })
    .catch((error) => {
      res.status(error.statusCode).send(error.error.error_description);
    });

  } else {
    res.status(400).send('Required parameters missing');
  }
  
  users.updateOne({"_id" : ObjectId(userId)}, {$push: {"store": shop}});


});

app.get('/api/shop', (req, res) => {
  const shopRequestUrl = 'https://' + store + '/admin/shop.json';
      const shopRequestHeaders = {
        'X-Shopify-Access-Token': access,
      };
      axios.get(shopRequestUrl, {
        headers: shopRequestHeaders,
       })      
       .then(function (response) {
         response = stringify(response);
        res.send(response);
      })
      .catch(function (error) {
        console.log(error);
      });
}) 
  

app.get('/api/customers', (req, res) => {
  const shopRequestUrl = 'https://' + store + '/admin/customers.json';
  const shopRequestHeaders = {
    'X-Shopify-Access-Token': access,
  };
  axios.get(shopRequestUrl, {
    headers: shopRequestHeaders,
   })      
   .then(function (response) {
     response = stringify(response);
    res.send(response);
  })
  .catch(function (error) {
    console.log(error);
  });
});
  
   



app.get('/', ensureLoggedIn, function(req, res, next) {
  users.findOne({"_id" : ObjectId(req.user.id.slice(6))})
   .then(user => {
     store = user.store[0];
     access = user.access_token[0];
   });
  res.sendFile(path.join(__dirname+'/client/dist/index.html'));
});








const port = process.env.PORT || 5000;

app.use(express.static(path.join(__dirname, 'client/dist')));


MongoClient.connect(dbUrl)
.then(connection => {
  console.log("Connected successfully to server");
   db = connection.db(dbName);
   users = db.collection('users');
  app.listen(port, () => `Server running on port 5000`);
}).catch(error => {
  console.log('ERROR:', error);
});






