import React from 'react';
import './styles/Navigation.css';
import {
    NavLink
  } from "react-router-dom";

export default class Navigation extends React.Component {
    render() {
        return (
        
            <div className="navigation">
                <div className="logo">
                 <b>Convert</b>
                </div>
                <nav className="nav">
                   <NavLink to="/" className="nav-links"><i className="fa fa-home " aria-hidden="true"></i>Dashboard</NavLink>
                   <NavLink to="/create"  className="nav-links"><i className="fa fa-tag" aria-hidden="true"></i>Create</NavLink>
                   <NavLink to="/analytics"  className="nav-links">
                    <span><i className="fa fa-line-chart" aria-hidden="true"></i></span> 
                    Analytics</NavLink>
                    <NavLink to="/integrations"  className="nav-links"><i className="fa fa-cubes" aria-hidden="true"></i>Integrations</NavLink>
                    <NavLink to="/marketplace"  className="nav-links"><i className="fa fa-cart-plus" aria-hidden="true"></i>Marketplace</NavLink>
                    <NavLink to="/settings"  className="nav-links"><i className="fa fa-cog" aria-hidden="true"></i>Settings</NavLink>
                    <NavLink to="/support"  className="nav-links"><i className="fa fa-question-circle" aria-hidden="true"></i>Support</NavLink>
                </nav>
                <div className="logout">
                 <a><i> LOGOUT</i> </a>
                </div>
          </div>
        )
    }
}