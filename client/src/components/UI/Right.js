import React from 'react';
import './styles/Right.css';
import {
  Route
} from "react-router-dom";
import Dashboard from '../Content/Dashboard.js';
import Create from '../Content/Create.js';

export default class Right extends React.Component {
    render() {
        return (
          <div className="right">
            <div className="content">
             <Route exact path="/" component={Dashboard}/>
             <Route path="/create" component={Create}/>
            </div>
          </div>
        )
    }
}