import React from 'react';
import Navigation from './components/UI/Navigation.js'
import Right from './components/UI/Right.js'
import './App.css'
import {
    HashRouter
  } from "react-router-dom";

export default class App extends React.Component {
    render() {
        return (
        <HashRouter>
          <div className="container">
            <Navigation />
            <Right />
          </div>
          </HashRouter>
        )
    }
}